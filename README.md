# cv_slideshow
simple opencv slideshow for Linux and Windows (cross compilation only)

## Français (english version follows)

Simple slideshow : analysez une vidéo (de courte durée, par exemple 1min maxi), en naviguant image par image.

Copyright (c) 2016-2019 Eric Bachard  
Contact : voir info.png

Actuellement, il n'y a aucun besoin de fournir un bazar du type  makefile ou cmake pour une simple application 
qui sera utilisée en tant que plugin plus tard. C'est la raison pour laquelle un simple shell script (de type bash) 
est fourni pour aider à la création des binaires (pour ceux qui savent faire).

### Binaires

IMPORTANT : une version Windows (64 bits) est fournie par défaut, DANS LE SEUL BUT de permettre de tester l'application.


### Utilisation sous Windows (64 bits)

IMPORTANT : il est peut-être nécessaire de désactiver la protection de votre Windows (par exemple pour 10 min), ou de confirmer que vous souhaitez lancer l'application (seulement la première fois).

Principe:

Vérifiez d'abord que vous avez installé les bons codecs, par exemple pour pouvoir lire des .mp4 ou tout autre format vidéo.

Téléchargez l'archive installable ici : https://framagit.org/ericb/image_par_image/blob/master/Installable/slideshow1.0.exe

Pour l'installation : 
- double cliquez sur l'archive
- suivre les instructions, en particulier **penser à créer un raccourci sur le bureau** car très utile.

Tests :
- un double-click sur "slideshow" doit permettre d'ouvrir MyVideo0.avi, fourni par défaut
- pour analyser d'autres clips : glisser la vidéo choisie sur l'icon de slideshow, et le démarrage est automatique

La navigation peut être effectuée soit à l'aide du curseur, soit avec les flèches gauche/droit du clavier
(ou L/M si les flèches de navigation ne marchent pas)

Le curseur permet ainsi d'afficher l'image exacte de la vidéo que vous souhaitez analyser, avec un numéro d'image.

ESC : quitte la navigation et ferme le logiciel


### Usage sous Linux

compiler slideshow (après installation des dépendances, + exécuter le script)
Ensuite, placer le binaire slideshow dans un répertoire donné.
S'y placer et ouvrir un terminal

Pour exécuter l'application taper simplement la commande suivante :

./slideshow /path_to_a_video

=> si ffmpeg et les bons codecs sont installés, la vidéo passée en 2nd argument (le nom du script étant le premier),
celle-ci sera chargée et la première image trouvée sera affichée. La fenêtre peut être redimensionnée au besoin.

La navigation peut être faite comme sous Windows, c'est à dire soit à l'aide du curseur, soit avec les flèches gauche/droit du clavier
(ou L/M si les flèches de navigation ne marchent pas)

Le curseur permet ainsi d'afficher l'image exacte de la vidéo que vous souhaitez analyser, avec un numéro d'image.

ESC : quitte la navigation et ferme le logiciel


### création des binaires 

Il y a un simple shell script, utilisé pour les 2 cas à exécuter : cross_compile_for_Windows.sh

IMPORTANT : bien résoudre les dépendances avant de continuer (la liste sera complètée précisément dès que possible)

#### Cross compilation pour la version Windows

Cette opération est donc effectuée sous Linux, mais l'hôte cible est Windows 64 
Pour compiler la version Windows, décommenter la ligne TARGET=WINDOWS 
(décommenter signifie enlever le caractère # du début de ligne et enregistrer la modification)

#### Compilation de la version Linux

Si la ligne contenant TARGET=  est commentée, c'est la version Linux qui sera compilée.

1. résoudre et installer toutes les dépendances (merci de lire attentivement le script, tout est dedans)
2. ensuite exécutez la commande suivante  : ./cross_compile_for_Windows.sh

=> si tout s'est bien passé, il devrait y avoir un binaire nommé slideshow, et adapté à votre distribution Linux à la fin !


Have Fun !!

Reminder : please follow the instructions to solve the dependencies


### Cleanup

To remove the binaries you created, just type :
./cross_compile_for_Windows.sh clean


### License

Le script cross_compile_for_Windows.sh est fournie sous la licence GPL v2. Merci de lire: https://www.gnu.org/licenses/gpl-2.0.html

Le fichier slideshow.cpp est fourni sous la licence MIT. Merci de lire: https://opensource.org/licenses/MIT

C'est tout !
Dans le futur, ce slideshow sera intégré dans un logiciel appelé miniDart, dédié aux entraîneurs faisant de l'analyse vidéo (le Handball en ce qui me concerne ;-) ) en tant que plugin.

## English

Simple slideshow : navigate in a movie (short length, 1 min max advised), image by image

Copyright (c) 2016-2019 Eric Bachard  
See info.png to contact me

Currently, there is no need to provide a Makefile nor CMake bazaar for just a simple application who will 
be used as plugin later. That's the reason why a shell script (bash) is provided to simplify the binary creation,
for the people able to do that themselves.

### Binaries

IMPORTANT : there is a Windows version provided by default, for testing purpose ONLY.

### Usage on Windows

IMPORTANT : it will probably aksed you to confirm you want to use the application (only once), or to disable your Windows protection (e.g. say for 10 min).

Principle:
Verify you installed some codecs, e.g. to watch .mp4 or wav or some other video formats

Download the archive from there : https://framagit.org/ericb/image_par_image/blob/master/Installable/slideshow1.0.exe

To install : 
- double-clic on the archive
- follow the instructions, and **don't forget to create an application shortcut on the desktop** (very usefull).


Tests :
- double-click on slideshow should open MyVideo0.avi, provided for tetsing purpose
- to analyse other clips : Drag a video file over slideshow.exe icon should launch slideshow

The slideshow can be made either using the slider (located on top), or using left/right or L/M keys :

- the slider allows to display the exact image of the movie you want to see, as an image number ;
- -> or <- keys allow to change image by image too (if arrows don't work, L and M are normaly available for navigation).

ESC : quit the slideshow


### Usage on Linux

build slideshow (install dependencies, + run the script)
put slideshow in a given directory.
Open a terminal, and go in the one containing slideshow

Simply run slideshow, the following way :

./slideshow /path_to_a_video

=> if ffmpeg and the right codecs are installed, the video passed as 2nd argument (the script name is the first),
the video will be loaded and image1 will be displayed. The window can be resized to fit the screen.

The slideshow can be made either using the slider on top, or using the keyboard :
- the slider allows to display the exact image number you want to see; 
- -> or <- keys allow to change image by image too (if arrows don't work, L and M are normaly available for navigation)

ESC : quit the slideshow


### Binary creation

There is one little script to run : cross_compile_for_Windows.sh

IMPORTANT : there are several dependencies (the list will be completed precisely asap)

#### Cross compilation for Windows
To build a Windows version, uncomment the line TARGET=WINDOWS (uncomment means remove the # at the begining of the line)

#### Linux build

If the line containing TARGET=  is commented, the Linux version wil be built.

1. install all dependencies (read the script, please)
2. run it : ./cross_compile_for_Windows.sh

=> if nothing went wrong, there should be a slideshow binary adapted to your distro at the end

Have Fun !!

Reminder : please follow the instructions to solve the dependencies


### Cleanup

To remove the binaries you created, just type :
./cross_compile_for_Windows.sh clean


### License


The shell script is provided under the GPL v2 license. Please see: https://www.gnu.org/licenses/gpl-2.0.html

The file slideshow.cpp is provided under the MIT license. Please see https://opensource.org/licenses/MIT

That's all. In the future, this slideshow ill be integrated inside miniDart, a dedicated software for sport trainers (Handball in my case ;-) ), as a plugin.

-- 
ericb

