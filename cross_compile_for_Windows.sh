#!/bin/bash

# Author : Eric Bachard  © 2016-2018
# contact : see info.png file
# license : GPL V2
# see : http://www.gnu.org/licenses/gpl-2.0.html
#
# This little shell script will build the Windows 64 version on Linux (Debian like)
#
# Any suggestion, or improvement is welcome.
#
# A word about Mac OS X version :
# the code should build directly, just the SDK and some cooking for the build are probably needed.
# Waiting, there is no Apple version, because I refuse to pay for the SDK - pay to produce free software ? -
# and I don't want to use the App Store. This means, if ever some nice people wants to provide one,
# including visibly the origin 
# of the software  and the author, under the same license, there is absolutely no problem for me.

# Usage :
# don't forget to chmod it as executable : chmod ug+x cross_compile_for_Windows.sh
#
# Suggestion : edit this file using mc (Midnight Commander), or with any colored syntax featured editor
#
# Dependencies (sudo apt-get install ... ):
#   mingw32 for 64 version + everything around (apt-cache search mingw* is your friend)
#
#   Cross compilation (from Linux for Windows):
#     - opencv-3.1.0  (be careful : above versions have a lot of regressions)
#     - ffmpeg  (opencv_ffmpeg310_64.dll)
#     - maybe opengl32, libpng, libjpeg, libIlmImf and some other libs -mandatory for the cross-compilation-

# uncomment if you want to see what happens ;-)
#set -x

# commented => Linux build
# uncommented => Windows build (cross-compilation). Binaries have been tested OK on win7+
#TARGET=WINDOWS

# default installation path for cross compiler. Adapt to your needs
PREFIX=/usr/local/cross-tools/x86_64-w64-mingw32

export PATH="${PREFIX}/bin":$PATH
export PKG_CONFIG_PATH=${PREFIX}/lib/pkgconfig

if [ "x$1" == "xclean" ] ; then
   rm -f slideshow slideshow.exe 2>/dev/null
   echo -e "Binaries have been removed\n"
   exit
fi 

if [ "x$TARGET" == "xWINDOWS" ] ; then

    #############################################
    #  WINDOWS CROSS COMPILATION CONFIGURATION  #
    #############################################

    # the command line. Please notice the macroprocessor directive "#if defined _WIN32" (Visual Studio 2013+) 
    # vs WIN32 (2013 and previous version of VS). We use them to differentiate Windows / Linux builds.

    echo -e "Building Windows version\n"
    x86_64-w64-mingw32-g++ -D_WIN32 -DWIN32 -DWINDOWS_BUILD -std=c++11 -mwindows -o slideshow.exe slideshow1.cpp \
                            -I${PREFIX}/include \
                            -I${PREFIX}/include/opencv2 \
                            -L$PREFIX/lib \
                            `pkg-config --libs opencv_mingw32` \
                            -L/usr/local/cross-tools/x86_64-w64-mingw32/share/OpenCV/3rdparty/lib \
                            -llibjpeg \
                            -llibwebp \
                            -llibpng \
                            -llibtiff \
                            -llibjasper \
                            -lIlmImf \
                            -llibprotobuf \
                            -L/usr/x86_64-w64-mingw32/lib \
                            -lglu32 \
                            -lopengl32 \
                            -lz -static\
                            -lavifil32 \
                            -lavicap32 \
                            -lvfw32 \
                            -lcomdlg32 \
                            -lgdi32 \
                            -luser32 \
                            -lcomctl32 \
                            -lole32 \
                            -loleaut32 \
                            -luuid  \
                            -lturbojpeg \
                            -static-libgcc \
                            -static-libstdc++

elif [ "x$TARGET" != "xWINDOWS" ] ; then

    #################
    #  LINUX BUILD  #
    #################

    echo -e "Building Linux version\n"

    # REMINDER : you need OpenCV 3.1.0 at least !!
    # the simplest way to check whether opencv for development
    # is installed, is to enter the following command line :
    # pkg-config --cflags --libs opencv
    # If a prolem occurs, it means you forget to install 
    # either opencv or pkg-config or both ...

    # FIXME : add tests to check for pkg-config present or not

    g++ -Wall -std=c++11 -o slideshow slideshow1.cpp `pkg-config --cflags --libs opencv`

fi







