/*

    slideshow1.cpp

    MIT license:

    Copyright 2016-2018 Eric Bachard

    Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
    and associated documentation files (the "Software"), to deal in the Software without restriction,
    including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
    subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all copies or substantial
    portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
    LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH 
    THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

#include <opencv2/opencv.hpp>
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <vector>
#include <stdio.h>

#ifdef __WIN32
#include <windows.h>
#endif

#include <iostream>

#define DELAY_MIN  10
#define DELAY_MAX 200

using namespace cv;
using std::cout;
using std::endl;

short int getPriority(bool);
void MOUSE_CLICK(int, int, int, int, void *);

static bool b_clicked = false;
// initial values : we suppose we have a one frame only video
static int firstFrameNumber = 1;
static int lastFrameNumber = 1;
static int currentFrameNumber = 1;

static const std::string defaultFilename = "MyVideo0.avi";

int main(int argc, char** argv)
{

    // Don't forget, drag and drop works natively on Windows !
    // On Linux, you'll need to pass the filename as argument in the command line

    // set the default value to empty
    std::string currentVideo = "";

    if (argc<2)
        currentVideo = defaultFilename;

    else
        currentVideo = argv[1];

    namedWindow("Slideshow", CV_WINDOW_NORMAL);
    VideoCapture video(currentVideo, cv::CAP_FFMPEG);

    if (!video.isOpened())
      return -1;

    int frame_count  = video.get(CV_CAP_PROP_FRAME_COUNT);
    Mat frames[frame_count];
    int pos = firstFrameNumber;
    bool bStop = false;

    // FIXME : search FIRST the last usable frame. Safe, but uses too much of memory, and slow !! 
    // got a better algo in my files, but this one is curently crashing if ever the video file 
    // is not clean, e.g. has one corrupted frame ( :-/ )
    // last, use another thread is not that easy, because pos is expected to start the second part ...
    while (bStop == false )
    {
        pos = video.get(CV_CAP_PROP_POS_FRAMES);
#ifdef DEBUG
        std::cout << "pos = " << pos << std::endl;
#endif
        Mat f;
        video >> f;

        if (!f.empty())
        {
            frames[pos] = f;
            f.release();
        }
        else
            bStop = true;
    }

    // keep valid frames to avoid a bad surprise. Better reencode crappy video than crash
    lastFrameNumber = pos;

    if (lastFrameNumber < 1)
       lastFrameNumber = 1;

    namedWindow( "Slideshow",CV_WINDOW_NORMAL|CV_WINDOW_OPENGL );
    createTrackbar( "Image", "Slideshow", &currentFrameNumber, lastFrameNumber-1, 0);

    // must be the same name than the observed frame !
    cvSetMouseCallback("Slideshow", MOUSE_CLICK, 0);
    bool bQuit = false;

    while (!bQuit)
    {
      imshow("Slideshow", frames[currentFrameNumber]);
      int iRet = waitKey(getPriority(b_clicked));
#ifdef DEBUG
      std::cout << "Key : " << iRet << std::endl;
#endif
      /* previous image*/
      if (((iRet == 108 ) || (iRet == 2424832 )) && (currentFrameNumber > 1))
      {
         currentFrameNumber--;
         setTrackbarPos("Image", "Slideshow", currentFrameNumber);
      }
      /* next image*/
      if (((iRet == 109 ) || (iRet == 2555904 )) && (currentFrameNumber < (lastFrameNumber -1)))
      {
         currentFrameNumber++;
         setTrackbarPos("Image", "Slideshow", currentFrameNumber);
      }

      if (iRet == 27)
         bQuit = true;
    }
    // yes, this is a lot of ram, and we need to release it cleanely ...
    for (int i=0 ; i< pos; i++) frames[i].release();

    video.release();
    destroyWindow("Slideshow");
    return EXIT_SUCCESS;
}

short int getPriority( bool IsClicked)
{
    if (IsClicked)
        return DELAY_MIN;
    else
        return DELAY_MAX;
}

void MOUSE_CLICK(int event, int x, int y, int flags, void *param)
{
    switch(event)
    {
        case CV_EVENT_LBUTTONDOWN:
        {
            b_clicked = true;
        }
        break;

        case CV_EVENT_LBUTTONUP:
        {
            b_clicked = false;
        }
        break;

        default:
        break;
    }
}

